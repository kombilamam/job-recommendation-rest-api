# backend
### Project sepup
```
pip install flask
pip install flask-cors
pip install spacy
pip install spacy-lefff
python3 -m spacy download fr_core_news_sm
pip install sklearn
```
### Compiles 
```
python3 JobRec.py
```
# frontend
```
sudo apt-get install -y nodejs
sudo apt install npm

```
## Project setup
```
cd frontend
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
