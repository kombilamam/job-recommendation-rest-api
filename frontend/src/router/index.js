import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/JobRec',
    name: 'JobRec',

    component: () => import(/* webpackChunkName: "about" */ '../views/JobRec.vue')
  },
  {
    path: '/UserRec',
    name: 'UserRec',

    component: () => import(/* webpackChunkName: "about" */ '../views/UserRec.vue')
  },
  {
    path: '/Jobs',
    name: 'Jobs',

    component: () => import(/* webpackChunkName: "about" */ '../views/Jobs.vue')
  },
  {
    path: '/Jobbers',
    name: 'Jobbers',

    component: () => import(/* webpackChunkName: "about" */ '../views/Jobbers.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
